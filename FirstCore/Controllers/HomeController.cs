﻿using Microsoft.AspNetCore.Mvc;

namespace FirstCore.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
